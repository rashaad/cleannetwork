//
//  RequestConstructor.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 7/30/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

public protocol RequestDescriptor {
    // Default method is GET
    var method: Request.Method {get}
    var url: URL? {get}
    var cachePolicy: URLRequest.CachePolicy {get}
    var headerFields: [String: String] {get}
    var bodyFields: [String: Any] {get}
    var bodyData: Data? {get}
    // Request timeout duration (defaulted to 10 sec)
    var timeoutInterval: TimeInterval {get}
}

/*
 * Base class used for wrapping URLRequest
 */
open class Request: NSObject, RequestDescriptor {
    public enum Method: String {
        case post = "POST"
        case get = "GET"
        case delete = "DELETE"
        case put = "PUT"
    }
    
    open var method: Request.Method {
        return .get
    }
    
    open var qualityOfService: QualityOfService {
        return .default
    }
    
    open var priority: Operation.QueuePriority {
        return .normal
    }
    
    open var url: URL? {
        return .none
    }
    
    open var cachePolicy: URLRequest.CachePolicy {
        return .useProtocolCachePolicy
    }
    
    open var headerFields: [String: String] {
        return [String: String]()
    }
    
    open var bodyFields: [String: Any] {
        return [String: Any]()
    }
    
    open var bodyData: Data? {
        do {
            let data = try JSONSerialization.data(withJSONObject: bodyFields, options: .prettyPrinted)
            return data
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    open var timeoutInterval: TimeInterval {
        return 10.0
    }
    
    open var request: URLRequest? {
        guard let url = url else {
            return nil
        }
        
        var request = URLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
        headerFields.forEach {
            request.addValue($0.value, forHTTPHeaderField: $0.key)
        }
        
        if let bodyData = bodyData {
            request.httpBody = bodyData
        }
        
        request.httpMethod = method.rawValue
        
        return request
    }
}
