//
//  NetworkService.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 7/30/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

@discardableResult
public func dataTask(requestBuilder: Request, completion: DataTaskCompletionHandler?) -> DataTask {
    let dataTask = DataTask(requestBuilder: requestBuilder) { (data, urlResponse, error) in
        completion?(data, urlResponse, error)
    }
    
    execute(task: dataTask)
    
    return dataTask
}

fileprivate func execute<T>(task: NetworkTask<T>) {
    switch task.requestBuilder.qualityOfService {
    case .userInteractive:
        NetworkQueueManager.default.userInteractiveOperationQueue.addOperation(task)
    case .userInitiated:
        NetworkQueueManager.default.userInitiatedOperationQueue.addOperation(task)
    case .utility:
        NetworkQueueManager.default.utilityOperationQueue.addOperation(task)
    case .background:
        NetworkQueueManager.default.backgroundOperationQueue.addOperation(task)
    default:
        NetworkQueueManager.default.userInitiatedOperationQueue.addOperation(task)
    }
}
