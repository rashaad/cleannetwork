//
//  NetworkTask.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 8/1/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

/*
 *  Encapsulates a network operation and handles the appropriate
 *  completion callback
 */
open class NetworkTask<CompletionType>: Operation {
    
    let requestBuilder: Request
    
    var completionHandler: CompletionType?
    
    init(requestBuilder: Request, completionHandler: CompletionType?) {
        
        self.requestBuilder = requestBuilder
        self.completionHandler = completionHandler
        
        super.init()
        
        self.qualityOfService = requestBuilder.qualityOfService
        self.queuePriority = requestBuilder.priority
        
        guard let urlString = requestBuilder.request?.url?.absoluteString else {
            return
        }
        
        self.name = urlString
    }
}
