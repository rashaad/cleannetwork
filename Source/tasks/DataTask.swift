//
//  DataTask.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 8/1/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

public typealias DataTaskCompletionHandler = (Data?, URLResponse?, Error?) -> Void

open class DataTask: NetworkTask<DataTaskCompletionHandler> {
    
    fileprivate typealias DataTaskResponse = (data: Data?, urlResponse: URLResponse?, error: Error?)
    
    // Upon completion call back the completion handler
    fileprivate var dataTaskResponse: DataTaskResponse? {
        didSet {
            guard dataTaskResponse != nil else { return }
            completionHandler?(dataTaskResponse!.data, dataTaskResponse!.urlResponse, dataTaskResponse!.error)
        }
    }
    
    override open func main() {
        
        guard let request = requestBuilder.request else {
            return
        }
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        let task = SessionManager.default.session.dataTask(with: request) { [weak self, weak dispatchGroup] (data, urlResponse, error) in
            self?.dataTaskResponse = DataTaskResponse(data: data, urlResponse: urlResponse, error: error)
            dispatchGroup?.leave()
        }
        
        task.resume()
        
        // Block the operation until the request completes
        dispatchGroup.wait()
    }
    
}
