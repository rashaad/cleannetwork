//
//  NetworkQueueManager.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 8/1/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

/*
 *  Singleton used for managing the different queue types
 *  so that QOS can be properly reached
 */
internal class NetworkQueueManager {
    
    static let `default` = NetworkQueueManager()
    
    lazy var userInteractiveOperationQueue: OperationQueue = {
        let networkOperationQueue = OperationQueue()
        networkOperationQueue.maxConcurrentOperationCount = 20
        networkOperationQueue.qualityOfService = .userInitiated
        return networkOperationQueue
    }()
    
    lazy var userInitiatedOperationQueue: OperationQueue = {
        let networkOperationQueue = OperationQueue()
        networkOperationQueue.maxConcurrentOperationCount = 20
        networkOperationQueue.qualityOfService = .userInitiated
        return networkOperationQueue
    }()
    
    lazy var utilityOperationQueue: OperationQueue = {
        let networkOperationQueue = OperationQueue()
        networkOperationQueue.maxConcurrentOperationCount = 20
        networkOperationQueue.qualityOfService = .utility
        return networkOperationQueue
    }()
    
    lazy var backgroundOperationQueue: OperationQueue = {
        let networkOperationQueue = OperationQueue()
        networkOperationQueue.maxConcurrentOperationCount = 20
        networkOperationQueue.qualityOfService = .background
        return networkOperationQueue
    }()
    
}
