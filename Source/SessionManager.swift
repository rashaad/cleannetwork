//
//  SessionManager.swift
//  NetworkingFramework
//
//  Created by Rashaad Ramdeen on 8/1/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import Foundation

open class SessionManager {
    
    public static let `default`: SessionManager = {
        return SessionManager(configuration: URLSessionConfiguration.default)
    }()
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }
}
