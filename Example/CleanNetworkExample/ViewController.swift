//
//  ViewController.swift
//  CleanNetworkExample
//
//  Created by Rashaad Ramdeen on 8/2/18.
//  Copyright © 2018 Rashaad Ramdeen. All rights reserved.
//

import UIKit
import CleanNetwork

class TodoRequest: Request {
    override var url: URL {
        return URL(string: "https://jsonplaceholder.typicode.com/todos/1")!
    }
}

class ViewController:UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didPressButton(sender: UIButton) {
        
        CleanNetwork.dataTask(requestBuilder: TodoRequest()) { (data, response, error) in
            guard data != nil else {
                return
            }
            
            let responseString = String(data: data!, encoding: .utf8)
            print(responseString)
        }
    }

}

