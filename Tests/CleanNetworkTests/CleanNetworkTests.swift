import XCTest
@testable import CleanNetwork

final class CleanNetworkTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CleanNetwork().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
