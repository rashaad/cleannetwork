import XCTest

import CleanNetworkTests

var tests = [XCTestCaseEntry]()
tests += CleanNetworkTests.allTests()
XCTMain(tests)